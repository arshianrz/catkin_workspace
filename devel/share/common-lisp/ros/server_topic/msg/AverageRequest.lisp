; Auto-generated. Do not edit!


(cl:in-package server_topic-msg)


;//! \htmlinclude AverageRequest.msg.html

(cl:defclass <AverageRequest> (roslisp-msg-protocol:ros-message)
  ((A
    :reader A
    :initarg :A
    :type cl:float
    :initform 0.0))
)

(cl:defclass AverageRequest (<AverageRequest>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <AverageRequest>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'AverageRequest)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name server_topic-msg:<AverageRequest> is deprecated: use server_topic-msg:AverageRequest instead.")))

(cl:ensure-generic-function 'A-val :lambda-list '(m))
(cl:defmethod A-val ((m <AverageRequest>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader server_topic-msg:A-val is deprecated.  Use server_topic-msg:A instead.")
  (A m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <AverageRequest>) ostream)
  "Serializes a message object of type '<AverageRequest>"
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'A))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <AverageRequest>) istream)
  "Deserializes a message object of type '<AverageRequest>"
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'A) (roslisp-utils:decode-single-float-bits bits)))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<AverageRequest>)))
  "Returns string type for a message object of type '<AverageRequest>"
  "server_topic/AverageRequest")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'AverageRequest)))
  "Returns string type for a message object of type 'AverageRequest"
  "server_topic/AverageRequest")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<AverageRequest>)))
  "Returns md5sum for a message object of type '<AverageRequest>"
  "86dc5e57feab1a2b50e6db6b5a647d08")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'AverageRequest)))
  "Returns md5sum for a message object of type 'AverageRequest"
  "86dc5e57feab1a2b50e6db6b5a647d08")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<AverageRequest>)))
  "Returns full string definition for message of type '<AverageRequest>"
  (cl:format cl:nil "float32 A~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'AverageRequest)))
  "Returns full string definition for message of type 'AverageRequest"
  (cl:format cl:nil "float32 A~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <AverageRequest>))
  (cl:+ 0
     4
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <AverageRequest>))
  "Converts a ROS message object to a list"
  (cl:list 'AverageRequest
    (cl:cons ':A (A msg))
))
