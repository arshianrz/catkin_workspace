
(cl:in-package :asdf)

(defsystem "server_topic-msg"
  :depends-on (:roslisp-msg-protocol :roslisp-utils )
  :components ((:file "_package")
    (:file "AverageRequest" :depends-on ("_package_AverageRequest"))
    (:file "_package_AverageRequest" :depends-on ("_package"))
    (:file "AverageRequest" :depends-on ("_package_AverageRequest"))
    (:file "_package_AverageRequest" :depends-on ("_package"))
    (:file "AverageResponse" :depends-on ("_package_AverageResponse"))
    (:file "_package_AverageResponse" :depends-on ("_package"))
    (:file "AverageResponse" :depends-on ("_package_AverageResponse"))
    (:file "_package_AverageResponse" :depends-on ("_package"))
  ))