; Auto-generated. Do not edit!


(cl:in-package server_topic-msg)


;//! \htmlinclude AverageResponse.msg.html

(cl:defclass <AverageResponse> (roslisp-msg-protocol:ros-message)
  ((Average
    :reader Average
    :initarg :Average
    :type cl:float
    :initform 0.0))
)

(cl:defclass AverageResponse (<AverageResponse>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <AverageResponse>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'AverageResponse)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name server_topic-msg:<AverageResponse> is deprecated: use server_topic-msg:AverageResponse instead.")))

(cl:ensure-generic-function 'Average-val :lambda-list '(m))
(cl:defmethod Average-val ((m <AverageResponse>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader server_topic-msg:Average-val is deprecated.  Use server_topic-msg:Average instead.")
  (Average m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <AverageResponse>) ostream)
  "Serializes a message object of type '<AverageResponse>"
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'Average))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <AverageResponse>) istream)
  "Deserializes a message object of type '<AverageResponse>"
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'Average) (roslisp-utils:decode-single-float-bits bits)))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<AverageResponse>)))
  "Returns string type for a message object of type '<AverageResponse>"
  "server_topic/AverageResponse")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'AverageResponse)))
  "Returns string type for a message object of type 'AverageResponse"
  "server_topic/AverageResponse")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<AverageResponse>)))
  "Returns md5sum for a message object of type '<AverageResponse>"
  "9c8af30caa9fe2d18ffa64ac66eccc9c")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'AverageResponse)))
  "Returns md5sum for a message object of type 'AverageResponse"
  "9c8af30caa9fe2d18ffa64ac66eccc9c")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<AverageResponse>)))
  "Returns full string definition for message of type '<AverageResponse>"
  (cl:format cl:nil "float32 Average~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'AverageResponse)))
  "Returns full string definition for message of type 'AverageResponse"
  (cl:format cl:nil "float32 Average~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <AverageResponse>))
  (cl:+ 0
     4
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <AverageResponse>))
  "Converts a ROS message object to a list"
  (cl:list 'AverageResponse
    (cl:cons ':Average (Average msg))
))
