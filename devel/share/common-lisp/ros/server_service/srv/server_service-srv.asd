
(cl:in-package :asdf)

(defsystem "server_service-srv"
  :depends-on (:roslisp-msg-protocol :roslisp-utils )
  :components ((:file "_package")
    (:file "feedback" :depends-on ("_package_feedback"))
    (:file "_package_feedback" :depends-on ("_package"))
  ))