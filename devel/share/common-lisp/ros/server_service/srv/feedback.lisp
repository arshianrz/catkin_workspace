; Auto-generated. Do not edit!


(cl:in-package server_service-srv)


;//! \htmlinclude feedback-request.msg.html

(cl:defclass <feedback-request> (roslisp-msg-protocol:ros-message)
  ((ask
    :reader ask
    :initarg :ask
    :type cl:float
    :initform 0.0))
)

(cl:defclass feedback-request (<feedback-request>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <feedback-request>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'feedback-request)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name server_service-srv:<feedback-request> is deprecated: use server_service-srv:feedback-request instead.")))

(cl:ensure-generic-function 'ask-val :lambda-list '(m))
(cl:defmethod ask-val ((m <feedback-request>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader server_service-srv:ask-val is deprecated.  Use server_service-srv:ask instead.")
  (ask m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <feedback-request>) ostream)
  "Serializes a message object of type '<feedback-request>"
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'ask))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <feedback-request>) istream)
  "Deserializes a message object of type '<feedback-request>"
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'ask) (roslisp-utils:decode-single-float-bits bits)))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<feedback-request>)))
  "Returns string type for a service object of type '<feedback-request>"
  "server_service/feedbackRequest")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'feedback-request)))
  "Returns string type for a service object of type 'feedback-request"
  "server_service/feedbackRequest")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<feedback-request>)))
  "Returns md5sum for a message object of type '<feedback-request>"
  "9f381f7e5fa9f94e8894e610646f9da0")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'feedback-request)))
  "Returns md5sum for a message object of type 'feedback-request"
  "9f381f7e5fa9f94e8894e610646f9da0")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<feedback-request>)))
  "Returns full string definition for message of type '<feedback-request>"
  (cl:format cl:nil "float32 ask~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'feedback-request)))
  "Returns full string definition for message of type 'feedback-request"
  (cl:format cl:nil "float32 ask~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <feedback-request>))
  (cl:+ 0
     4
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <feedback-request>))
  "Converts a ROS message object to a list"
  (cl:list 'feedback-request
    (cl:cons ':ask (ask msg))
))
;//! \htmlinclude feedback-response.msg.html

(cl:defclass <feedback-response> (roslisp-msg-protocol:ros-message)
  ((answer
    :reader answer
    :initarg :answer
    :type cl:float
    :initform 0.0))
)

(cl:defclass feedback-response (<feedback-response>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <feedback-response>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'feedback-response)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name server_service-srv:<feedback-response> is deprecated: use server_service-srv:feedback-response instead.")))

(cl:ensure-generic-function 'answer-val :lambda-list '(m))
(cl:defmethod answer-val ((m <feedback-response>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader server_service-srv:answer-val is deprecated.  Use server_service-srv:answer instead.")
  (answer m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <feedback-response>) ostream)
  "Serializes a message object of type '<feedback-response>"
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'answer))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <feedback-response>) istream)
  "Deserializes a message object of type '<feedback-response>"
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'answer) (roslisp-utils:decode-single-float-bits bits)))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<feedback-response>)))
  "Returns string type for a service object of type '<feedback-response>"
  "server_service/feedbackResponse")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'feedback-response)))
  "Returns string type for a service object of type 'feedback-response"
  "server_service/feedbackResponse")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<feedback-response>)))
  "Returns md5sum for a message object of type '<feedback-response>"
  "9f381f7e5fa9f94e8894e610646f9da0")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'feedback-response)))
  "Returns md5sum for a message object of type 'feedback-response"
  "9f381f7e5fa9f94e8894e610646f9da0")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<feedback-response>)))
  "Returns full string definition for message of type '<feedback-response>"
  (cl:format cl:nil "float32 answer~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'feedback-response)))
  "Returns full string definition for message of type 'feedback-response"
  (cl:format cl:nil "float32 answer~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <feedback-response>))
  (cl:+ 0
     4
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <feedback-response>))
  "Converts a ROS message object to a list"
  (cl:list 'feedback-response
    (cl:cons ':answer (answer msg))
))
(cl:defmethod roslisp-msg-protocol:service-request-type ((msg (cl:eql 'feedback)))
  'feedback-request)
(cl:defmethod roslisp-msg-protocol:service-response-type ((msg (cl:eql 'feedback)))
  'feedback-response)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'feedback)))
  "Returns string type for a service object of type '<feedback>"
  "server_service/feedback")