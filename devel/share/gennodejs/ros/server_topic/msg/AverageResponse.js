// Auto-generated. Do not edit!

// (in-package server_topic.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;

//-----------------------------------------------------------

class AverageResponse {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.Average = null;
    }
    else {
      if (initObj.hasOwnProperty('Average')) {
        this.Average = initObj.Average
      }
      else {
        this.Average = 0.0;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type AverageResponse
    // Serialize message field [Average]
    bufferOffset = _serializer.float32(obj.Average, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type AverageResponse
    let len;
    let data = new AverageResponse(null);
    // Deserialize message field [Average]
    data.Average = _deserializer.float32(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    return 4;
  }

  static datatype() {
    // Returns string type for a message object
    return 'server_topic/AverageResponse';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '9c8af30caa9fe2d18ffa64ac66eccc9c';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    float32 Average
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new AverageResponse(null);
    if (msg.Average !== undefined) {
      resolved.Average = msg.Average;
    }
    else {
      resolved.Average = 0.0
    }

    return resolved;
    }
};

module.exports = AverageResponse;
