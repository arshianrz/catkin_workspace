// Auto-generated. Do not edit!

// (in-package server_service.srv)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;

//-----------------------------------------------------------


//-----------------------------------------------------------

class feedbackRequest {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.ask = null;
    }
    else {
      if (initObj.hasOwnProperty('ask')) {
        this.ask = initObj.ask
      }
      else {
        this.ask = 0.0;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type feedbackRequest
    // Serialize message field [ask]
    bufferOffset = _serializer.float32(obj.ask, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type feedbackRequest
    let len;
    let data = new feedbackRequest(null);
    // Deserialize message field [ask]
    data.ask = _deserializer.float32(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    return 4;
  }

  static datatype() {
    // Returns string type for a service object
    return 'server_service/feedbackRequest';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return 'f9261947e131f30d34537fa04315afa9';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    float32 ask
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new feedbackRequest(null);
    if (msg.ask !== undefined) {
      resolved.ask = msg.ask;
    }
    else {
      resolved.ask = 0.0
    }

    return resolved;
    }
};

class feedbackResponse {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.answer = null;
    }
    else {
      if (initObj.hasOwnProperty('answer')) {
        this.answer = initObj.answer
      }
      else {
        this.answer = 0.0;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type feedbackResponse
    // Serialize message field [answer]
    bufferOffset = _serializer.float32(obj.answer, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type feedbackResponse
    let len;
    let data = new feedbackResponse(null);
    // Deserialize message field [answer]
    data.answer = _deserializer.float32(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    return 4;
  }

  static datatype() {
    // Returns string type for a service object
    return 'server_service/feedbackResponse';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '36a0b966f6f926103e5aacdad943727c';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    float32 answer
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new feedbackResponse(null);
    if (msg.answer !== undefined) {
      resolved.answer = msg.answer;
    }
    else {
      resolved.answer = 0.0
    }

    return resolved;
    }
};

module.exports = {
  Request: feedbackRequest,
  Response: feedbackResponse,
  md5sum() { return '9f381f7e5fa9f94e8894e610646f9da0'; },
  datatype() { return 'server_service/feedback'; }
};
