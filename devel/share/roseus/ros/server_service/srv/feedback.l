;; Auto-generated. Do not edit!


(when (boundp 'server_service::feedback)
  (if (not (find-package "SERVER_SERVICE"))
    (make-package "SERVER_SERVICE"))
  (shadow 'feedback (find-package "SERVER_SERVICE")))
(unless (find-package "SERVER_SERVICE::FEEDBACK")
  (make-package "SERVER_SERVICE::FEEDBACK"))
(unless (find-package "SERVER_SERVICE::FEEDBACKREQUEST")
  (make-package "SERVER_SERVICE::FEEDBACKREQUEST"))
(unless (find-package "SERVER_SERVICE::FEEDBACKRESPONSE")
  (make-package "SERVER_SERVICE::FEEDBACKRESPONSE"))

(in-package "ROS")





(defclass server_service::feedbackRequest
  :super ros::object
  :slots (_ask ))

(defmethod server_service::feedbackRequest
  (:init
   (&key
    ((:ask __ask) 0.0)
    )
   (send-super :init)
   (setq _ask (float __ask))
   self)
  (:ask
   (&optional __ask)
   (if __ask (setq _ask __ask)) _ask)
  (:serialization-length
   ()
   (+
    ;; float32 _ask
    4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; float32 _ask
       (sys::poke _ask (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; float32 _ask
     (setq _ask (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;;
   self)
  )

(defclass server_service::feedbackResponse
  :super ros::object
  :slots (_answer ))

(defmethod server_service::feedbackResponse
  (:init
   (&key
    ((:answer __answer) 0.0)
    )
   (send-super :init)
   (setq _answer (float __answer))
   self)
  (:answer
   (&optional __answer)
   (if __answer (setq _answer __answer)) _answer)
  (:serialization-length
   ()
   (+
    ;; float32 _answer
    4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; float32 _answer
       (sys::poke _answer (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; float32 _answer
     (setq _answer (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;;
   self)
  )

(defclass server_service::feedback
  :super ros::object
  :slots ())

(setf (get server_service::feedback :md5sum-) "9f381f7e5fa9f94e8894e610646f9da0")
(setf (get server_service::feedback :datatype-) "server_service/feedback")
(setf (get server_service::feedback :request) server_service::feedbackRequest)
(setf (get server_service::feedback :response) server_service::feedbackResponse)

(defmethod server_service::feedbackRequest
  (:response () (instance server_service::feedbackResponse :init)))

(setf (get server_service::feedbackRequest :md5sum-) "9f381f7e5fa9f94e8894e610646f9da0")
(setf (get server_service::feedbackRequest :datatype-) "server_service/feedbackRequest")
(setf (get server_service::feedbackRequest :definition-)
      "float32 ask
---
float32 answer
")

(setf (get server_service::feedbackResponse :md5sum-) "9f381f7e5fa9f94e8894e610646f9da0")
(setf (get server_service::feedbackResponse :datatype-) "server_service/feedbackResponse")
(setf (get server_service::feedbackResponse :definition-)
      "float32 ask
---
float32 answer
")



(provide :server_service/feedback "9f381f7e5fa9f94e8894e610646f9da0")


