;; Auto-generated. Do not edit!


(when (boundp 'server_topic::AverageRequest)
  (if (not (find-package "SERVER_TOPIC"))
    (make-package "SERVER_TOPIC"))
  (shadow 'AverageRequest (find-package "SERVER_TOPIC")))
(unless (find-package "SERVER_TOPIC::AVERAGEREQUEST")
  (make-package "SERVER_TOPIC::AVERAGEREQUEST"))

(in-package "ROS")
;;//! \htmlinclude AverageRequest.msg.html


(defclass server_topic::AverageRequest
  :super ros::object
  :slots (_A ))

(defmethod server_topic::AverageRequest
  (:init
   (&key
    ((:A __A) 0.0)
    )
   (send-super :init)
   (setq _A (float __A))
   self)
  (:A
   (&optional __A)
   (if __A (setq _A __A)) _A)
  (:serialization-length
   ()
   (+
    ;; float32 _A
    4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; float32 _A
       (sys::poke _A (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; float32 _A
     (setq _A (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;;
   self)
  )

(setf (get server_topic::AverageRequest :md5sum-) "86dc5e57feab1a2b50e6db6b5a647d08")
(setf (get server_topic::AverageRequest :datatype-) "server_topic/AverageRequest")
(setf (get server_topic::AverageRequest :definition-)
      "float32 A
")



(provide :server_topic/AverageRequest "86dc5e57feab1a2b50e6db6b5a647d08")


