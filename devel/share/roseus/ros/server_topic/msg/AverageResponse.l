;; Auto-generated. Do not edit!


(when (boundp 'server_topic::AverageResponse)
  (if (not (find-package "SERVER_TOPIC"))
    (make-package "SERVER_TOPIC"))
  (shadow 'AverageResponse (find-package "SERVER_TOPIC")))
(unless (find-package "SERVER_TOPIC::AVERAGERESPONSE")
  (make-package "SERVER_TOPIC::AVERAGERESPONSE"))

(in-package "ROS")
;;//! \htmlinclude AverageResponse.msg.html


(defclass server_topic::AverageResponse
  :super ros::object
  :slots (_Average ))

(defmethod server_topic::AverageResponse
  (:init
   (&key
    ((:Average __Average) 0.0)
    )
   (send-super :init)
   (setq _Average (float __Average))
   self)
  (:Average
   (&optional __Average)
   (if __Average (setq _Average __Average)) _Average)
  (:serialization-length
   ()
   (+
    ;; float32 _Average
    4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; float32 _Average
       (sys::poke _Average (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; float32 _Average
     (setq _Average (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;;
   self)
  )

(setf (get server_topic::AverageResponse :md5sum-) "9c8af30caa9fe2d18ffa64ac66eccc9c")
(setf (get server_topic::AverageResponse :datatype-) "server_topic/AverageResponse")
(setf (get server_topic::AverageResponse :definition-)
      "float32 Average
")



(provide :server_topic/AverageResponse "9c8af30caa9fe2d18ffa64ac66eccc9c")


