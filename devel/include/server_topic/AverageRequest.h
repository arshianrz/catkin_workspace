// Generated by gencpp from file server_topic/AverageRequest.msg
// DO NOT EDIT!


#ifndef SERVER_TOPIC_MESSAGE_AVERAGEREQUEST_H
#define SERVER_TOPIC_MESSAGE_AVERAGEREQUEST_H


#include <string>
#include <vector>
#include <map>

#include <ros/types.h>
#include <ros/serialization.h>
#include <ros/builtin_message_traits.h>
#include <ros/message_operations.h>


namespace server_topic
{
template <class ContainerAllocator>
struct AverageRequest_
{
  typedef AverageRequest_<ContainerAllocator> Type;

  AverageRequest_()
    : A(0.0)  {
    }
  AverageRequest_(const ContainerAllocator& _alloc)
    : A(0.0)  {
  (void)_alloc;
    }



   typedef float _A_type;
  _A_type A;





  typedef boost::shared_ptr< ::server_topic::AverageRequest_<ContainerAllocator> > Ptr;
  typedef boost::shared_ptr< ::server_topic::AverageRequest_<ContainerAllocator> const> ConstPtr;

}; // struct AverageRequest_

typedef ::server_topic::AverageRequest_<std::allocator<void> > AverageRequest;

typedef boost::shared_ptr< ::server_topic::AverageRequest > AverageRequestPtr;
typedef boost::shared_ptr< ::server_topic::AverageRequest const> AverageRequestConstPtr;

// constants requiring out of line definition



template<typename ContainerAllocator>
std::ostream& operator<<(std::ostream& s, const ::server_topic::AverageRequest_<ContainerAllocator> & v)
{
ros::message_operations::Printer< ::server_topic::AverageRequest_<ContainerAllocator> >::stream(s, "", v);
return s;
}

} // namespace server_topic

namespace ros
{
namespace message_traits
{



// BOOLTRAITS {'IsFixedSize': True, 'IsMessage': True, 'HasHeader': False}
// {'std_msgs': ['/opt/ros/melodic/share/std_msgs/cmake/../msg'], 'server_topic': ['/home/arshia/catkin_ws/src/server_topic/msg', '/home/arshia/catkin_ws/src/server_topic/msg']}

// !!!!!!!!!!! ['__class__', '__delattr__', '__dict__', '__doc__', '__eq__', '__format__', '__getattribute__', '__hash__', '__init__', '__module__', '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '__sizeof__', '__str__', '__subclasshook__', '__weakref__', '_parsed_fields', 'constants', 'fields', 'full_name', 'has_header', 'header_present', 'names', 'package', 'parsed_fields', 'short_name', 'text', 'types']




template <class ContainerAllocator>
struct IsFixedSize< ::server_topic::AverageRequest_<ContainerAllocator> >
  : TrueType
  { };

template <class ContainerAllocator>
struct IsFixedSize< ::server_topic::AverageRequest_<ContainerAllocator> const>
  : TrueType
  { };

template <class ContainerAllocator>
struct IsMessage< ::server_topic::AverageRequest_<ContainerAllocator> >
  : TrueType
  { };

template <class ContainerAllocator>
struct IsMessage< ::server_topic::AverageRequest_<ContainerAllocator> const>
  : TrueType
  { };

template <class ContainerAllocator>
struct HasHeader< ::server_topic::AverageRequest_<ContainerAllocator> >
  : FalseType
  { };

template <class ContainerAllocator>
struct HasHeader< ::server_topic::AverageRequest_<ContainerAllocator> const>
  : FalseType
  { };


template<class ContainerAllocator>
struct MD5Sum< ::server_topic::AverageRequest_<ContainerAllocator> >
{
  static const char* value()
  {
    return "86dc5e57feab1a2b50e6db6b5a647d08";
  }

  static const char* value(const ::server_topic::AverageRequest_<ContainerAllocator>&) { return value(); }
  static const uint64_t static_value1 = 0x86dc5e57feab1a2bULL;
  static const uint64_t static_value2 = 0x50e6db6b5a647d08ULL;
};

template<class ContainerAllocator>
struct DataType< ::server_topic::AverageRequest_<ContainerAllocator> >
{
  static const char* value()
  {
    return "server_topic/AverageRequest";
  }

  static const char* value(const ::server_topic::AverageRequest_<ContainerAllocator>&) { return value(); }
};

template<class ContainerAllocator>
struct Definition< ::server_topic::AverageRequest_<ContainerAllocator> >
{
  static const char* value()
  {
    return "float32 A\n"
;
  }

  static const char* value(const ::server_topic::AverageRequest_<ContainerAllocator>&) { return value(); }
};

} // namespace message_traits
} // namespace ros

namespace ros
{
namespace serialization
{

  template<class ContainerAllocator> struct Serializer< ::server_topic::AverageRequest_<ContainerAllocator> >
  {
    template<typename Stream, typename T> inline static void allInOne(Stream& stream, T m)
    {
      stream.next(m.A);
    }

    ROS_DECLARE_ALLINONE_SERIALIZER
  }; // struct AverageRequest_

} // namespace serialization
} // namespace ros

namespace ros
{
namespace message_operations
{

template<class ContainerAllocator>
struct Printer< ::server_topic::AverageRequest_<ContainerAllocator> >
{
  template<typename Stream> static void stream(Stream& s, const std::string& indent, const ::server_topic::AverageRequest_<ContainerAllocator>& v)
  {
    s << indent << "A: ";
    Printer<float>::stream(s, indent + "  ", v.A);
  }
};

} // namespace message_operations
} // namespace ros

#endif // SERVER_TOPIC_MESSAGE_AVERAGEREQUEST_H
