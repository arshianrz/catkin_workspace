# CMake generated Testfile for 
# Source directory: /home/arshia/catkin_ws/src
# Build directory: /home/arshia/catkin_ws/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("gtest")
subdirs("server_service")
subdirs("client_service")
subdirs("server_topic")
subdirs("client_topic")
subdirs("joystick_dynamixel")
subdirs("learning_joy")
