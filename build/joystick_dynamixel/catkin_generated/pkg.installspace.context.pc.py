# generated from catkin/cmake/template/pkg.context.pc.in
CATKIN_PACKAGE_PREFIX = ""
PROJECT_PKG_CONFIG_INCLUDE_DIRS = "/home/arshia/catkin_ws/install/include".split(';') if "/home/arshia/catkin_ws/install/include" != "" else []
PROJECT_CATKIN_DEPENDS = "geometry_msgs;message_generation;message_runtime;roscpp;sensor_msgs;std_msgs".replace(';', ' ')
PKG_CONFIG_LIBRARIES_WITH_PREFIX = "-ljoystick_dynamixel".split(';') if "-ljoystick_dynamixel" != "" else []
PROJECT_NAME = "joystick_dynamixel"
PROJECT_SPACE_DIR = "/home/arshia/catkin_ws/install"
PROJECT_VERSION = "0.0.0"
