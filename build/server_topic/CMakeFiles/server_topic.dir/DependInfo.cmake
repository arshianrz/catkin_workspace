# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/arshia/catkin_ws/src/server_topic/src/main.cpp" "/home/arshia/catkin_ws/build/server_topic/CMakeFiles/server_topic.dir/src/main.cpp.o"
  "/home/arshia/catkin_ws/src/server_topic/src/server.cpp" "/home/arshia/catkin_ws/build/server_topic/CMakeFiles/server_topic.dir/src/server.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "ROSCONSOLE_BACKEND_LOG4CXX"
  "ROS_BUILD_SHARED_LIBS=1"
  "ROS_PACKAGE_NAME=\"server_topic\""
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/home/arshia/catkin_ws/devel/include"
  "/home/arshia/catkin_ws/src/server_topic/include"
  "/opt/ros/melodic/include"
  "/opt/ros/melodic/share/xmlrpcpp/cmake/../../../include/xmlrpcpp"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
