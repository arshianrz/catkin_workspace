set(_CATKIN_CURRENT_PACKAGE "server_topic")
set(server_topic_VERSION "0.0.0")
set(server_topic_MAINTAINER "arshia <arshia@todo.todo>")
set(server_topic_PACKAGE_FORMAT "2")
set(server_topic_BUILD_DEPENDS "message_generation" "message_generation" "roscpp" "std_msgs" "server_topic_msg")
set(server_topic_BUILD_EXPORT_DEPENDS "roscpp" "std_msgs")
set(server_topic_BUILDTOOL_DEPENDS "catkin")
set(server_topic_BUILDTOOL_EXPORT_DEPENDS )
set(server_topic_EXEC_DEPENDS "message_runtime" "roscpp" "std_msgs" "server_topic_msg")
set(server_topic_RUN_DEPENDS "message_runtime" "roscpp" "std_msgs" "server_topic_msg")
set(server_topic_TEST_DEPENDS )
set(server_topic_DOC_DEPENDS )
set(server_topic_URL_WEBSITE "")
set(server_topic_URL_BUGTRACKER "")
set(server_topic_URL_REPOSITORY "")
set(server_topic_DEPRECATED "")