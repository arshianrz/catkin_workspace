#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables
export CMAKE_PREFIX_PATH="/home/arshia/catkin_ws/src/build-joystick_dynamixel-Desktop_Qt_5_12_1_GCC_64bit-Default/devel:/home/arshia/catkin_ws/devel:/opt/ros/melodic"
export LD_LIBRARY_PATH="/home/arshia/catkin_ws/src/build-joystick_dynamixel-Desktop_Qt_5_12_1_GCC_64bit-Default/devel/lib:/home/arshia/catkin_ws/devel/lib:/opt/ros/melodic/lib"
export PKG_CONFIG_PATH="/home/arshia/catkin_ws/src/build-joystick_dynamixel-Desktop_Qt_5_12_1_GCC_64bit-Default/devel/lib/pkgconfig:/home/arshia/catkin_ws/devel/lib/pkgconfig:/opt/ros/melodic/lib/pkgconfig"
export PYTHONPATH="/home/arshia/catkin_ws/src/build-joystick_dynamixel-Desktop_Qt_5_12_1_GCC_64bit-Default/devel/lib/python2.7/dist-packages:/home/arshia/catkin_ws/devel/lib/python2.7/dist-packages:/opt/ros/melodic/lib/python2.7/dist-packages"
export ROSLISP_PACKAGE_DIRECTORIES="/home/arshia/catkin_ws/src/build-joystick_dynamixel-Desktop_Qt_5_12_1_GCC_64bit-Default/devel/share/common-lisp:/home/arshia/catkin_ws/devel/share/common-lisp"
export ROS_DISTRO="melodic"
export ROS_ETC_DIR="/opt/ros/melodic/etc/ros"
export ROS_MASTER_URI="http://localhost:11311"
export ROS_PACKAGE_PATH="/home/arshia/catkin_ws/src/joystick_dynamixel:/home/arshia/catkin_ws/src:/opt/ros/melodic/share"
export ROS_PYTHON_VERSION="2"
export ROS_ROOT="/opt/ros/melodic/share/ros"
export ROS_VERSION="1"

# modified environment variables
export PATH="/opt/ros/melodic/bin:$PATH"
export PWD="/home/arshia/catkin_ws/src/build-joystick_dynamixel-Desktop_Qt_5_12_1_GCC_64bit-Default"